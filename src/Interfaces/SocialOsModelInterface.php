<?php

namespace Login\SocialOs\Interfaces;

interface SocialOsModelInterface
{
    public static function getDriver();
}