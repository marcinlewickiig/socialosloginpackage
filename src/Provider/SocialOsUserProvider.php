<?php
/**
 * Created by PhpStorm.
 * User: Paweł Cudziło
 * Date: 2016-03-31
 * Time: 14:43
 */

namespace Login\SocialOs\Provider;

use App\Clients\SocialOs;
use Cache;
use App\User;
use App\UserPoa;
use Carbon\Carbon;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Login\SocialOs\Models\SocialOsUser;


class SocialOsUserProvider implements UserProvider
{

    protected function getDriver()
    {
        return new SocialOs();
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        if (true) {
            return SocialOsUser::createModelByAccesToken($identifier);
        }
        return null;
    }

    /**
     * Retrieve a user by by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        dd($identifier, $token, 'token');
        // TODO: Implement retrieveByToken() method.
        $qry = UserPoa::where('admin_id', '=', $identifier)->where('remember_token', '=', $token);

        if ($qry->count() > 0) {
            $user = $qry->select('admin_id', 'username', 'first_name', 'last_name', 'email', 'password')->first();

            $attributes = array(
                'id' => $user->admin_id,
                'username' => $user->username,
                'password' => $user->password,
                'name' => $user->first_name . ' ' . $user->last_name,
            );

            return $user;
        }
        return null;


    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {

//        dd($user, $token,'updateRememberToken');
        // TODO: Implement updateRememberToken() method.
        $user->setRememberToken($token);

        //  $user->save();

    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
//@todo: What will happen when two people login with one account ?
        $code = $credentials['code'];
        return SocialOsUser::createModelByCode($code);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
//        dd($user, $credentials);
        return true;
    }
}