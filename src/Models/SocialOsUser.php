<?php
/**
 * Created by PhpStorm.
 * User: Paweł Cudziło
 * Date: 2016-04-01
 * Time: 09:16
 */


namespace Login\SocialOs\Models;

use App\Clients\SocialOs;
use Login\SocialOs\Interfaces\SocialOsModelInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SocialOsUser extends Authenticatable implements SocialOsModelInterface
{


    /**
     * store connection data
     * @var array
     */
    public $soicalData;

    /**
     * set up ouer primary key for social os key to store access token
     * @var string
     */
    protected $primaryKey = 'session_id';
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'session_id' => 'object',
    ];

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var SocialOs
     */
    protected $socialOsdriver = null;


    /**
     * @param $data
     */

    public function addData($data)
    {
        $this->soicalData = $data;
    }

    /**
     * @return SocialOs
     */

    public static function getDriver()
    {
        $driver = new SocialOs();
        return $driver;
    }

    /**
     * @param array $sessionId
     * @return $this
     */

    public function setSessionId(\stdClass $sessionId)
    {
        $this->session_id = $sessionId;
        return $this;
    }

    /**
     * @param $code
     * @return SocialOsUser
     */

    public static function createModelByCode($code)
    {
        $driver = self::getDriver();
        $userModel = new self();

        $userModel->setSessionId($driver->token($code));
        //fetch some more data
        return $userModel;
    }

    public static function createModelByAccesToken(\stdClass $token)
    {
        $driver = self::getDriver();
        $userModel = new self();
        $userModel->setSessionId($token);
        //fetch some more data
        return $userModel;
    }
}
